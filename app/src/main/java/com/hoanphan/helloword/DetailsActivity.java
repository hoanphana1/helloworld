package com.hoanphan.helloword;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    private TextView tvCounter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        tvCounter = findViewById(R.id.tv_counter);
        Intent intent = getIntent();
        if(intent != null){
            String data = intent.getStringExtra("data");
            tvCounter.setText(data);
        }
    }
}